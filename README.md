# Zabbix-linuxupdates

> Autor: Davis Mendoza Paco **<davis.men.pa@gmail.com> / <davis.mp@yandex.com>**

Este es una plantilla de Zabbix para monitorear actualizaciones de Sistemas operativos Gnu/Linux.

Ha sido probado en Zabbix v5.2.7 con Centos 7 y Debian 10 y 11. 

## Características

- Funciona con agentes activos
- Comprueba si hay actualizaciones e informa los números de cada una.
- Informa la fecha de las últimas actualizaciones.
- Informa si hay un reinicio pendiente por actualización de kernel.
- Incluye un panel para el Tablero.
- Incluye disparadores (Triggers) para advertir sobre los estados de actualización.

## Requisitos

- Solo se ha probado con agentes activos Zabbix.
- Tener instalado zabbix-sender

## Archivos incluidos

- **LinupdatesV5.xml:** Plantilla para importar a zabbix
- **check_updates-v5.sh:** script schell, Debe colocarse en el directorio de "plugins" en la carpeta de instalación de Zabbix Agent 

## Instalación

Crear el directorio de plugins
```sh
sudo mkdir /etc/zabbix/plugins
```

Descargar el script y copiar a la carpeta creada
```sh
mv check_updates-v5.sh /etc/zabbix/plugins
```

Asignarle permisos de ejecución al script creado
```sh
chmod 755 /etc/zabbix/plugins/check_updates-v5.sh
```

Cambiando el propietario de la carpeta y de su contenido
```sh
sudo chown -R zabbix.zabbix /etc/zabbix/plugins
```

Instalando Zabbix-sender

**Debian**

```sh
sudo apt install curl zabbix-sender
```

**CentOS 7**

```sh
yum install curl
wget https://repo.zabbix.com/zabbix/3.0/rhel/7/x86_64/zabbix-sender-3.0.32-1.el7.x86_64.rpm
rpm -i zabbix-sender-3.0.32-1.el7.x86_64.rpm
```

Adicionar un Job para revisar las actualizaciones
```sh
sudo crontab -e

0 06,12 * * * yum clean all & yum check-update
```

## Configuración

Habilitar la ejecución de comandos remotos 

> https://www.zabbix.com/documentation/5.2/en/manual/config/items/restrict_checks
 
Editar el archivo de configuración
```sh
sudo nano /etc/zabbix/zabbix_agent2.conf
```

Adicionar la siguiente linea en la configuración
```
AllowKey=system.run[*]
```

Para que los cambios surtan efecto, reiniciar el servicio
```sh
sudo systemctl restart zabbix-agent2.service
```

## Probar 

Para verificar si el script funciona correctamente, realizar la prueba ejecutando
```sh
sudo -u zabbix -H bash /etc/zabbix/plugins/check_updates-v5.sh
```

El resultado del comando nos muestra lo siguiente:
```sh
zabbix_sender [14058]: DEBUG: answer [{"response":"success","info":"processed: 1; failed: 0; total: 1; seconds spent: 0.000057"}]
info from server: "processed: 1; failed: 0; total: 1; seconds spent: 0.000057"
sent: 1; skipped: 0; total: 1
zabbix_sender [14060]: DEBUG: answer [{"response":"success","info":"processed: 1; failed: 0; total: 1; seconds spent: 0.000055"}]
info from server: "processed: 1; failed: 0; total: 1; seconds spent: 0.000055"
sent: 1; skipped: 0; total: 1
zabbix_sender [14062]: DEBUG: answer [{"response":"success","info":"processed: 1; failed: 0; total: 1; seconds spent: 0.000041"}]
info from server: "processed: 1; failed: 0; total: 1; seconds spent: 0.000041"
sent: 1; skipped: 0; total: 1
zabbix_sender [14064]: DEBUG: answer [{"response":"success","info":"processed: 1; failed: 0; total: 1; seconds spent: 0.000067"}]
info from server: "processed: 1; failed: 0; total: 1; seconds spent: 0.000067"
sent: 1; skipped: 0; total: 1
zabbix_sender [14066]: DEBUG: answer [{"response":"success","info":"processed: 1; failed: 0; total: 1; seconds spent: 0.000066"}]
info from server: "processed: 1; failed: 0; total: 1; seconds spent: 0.000066"
sent: 1; skipped: 0; total: 1
zabbix_sender [14068]: DEBUG: answer [{"response":"success","info":"processed: 1; failed: 0; total: 1; seconds spent: 0.000054"}]
info from server: "processed: 1; failed: 0; total: 1; seconds spent: 0.000054"
sent: 1; skipped: 0; total: 1
zabbix_sender [14070]: DEBUG: answer [{"response":"success","info":"processed: 1; failed: 0; total: 1; seconds spent: 0.000041"}]
info from server: "processed: 1; failed: 0; total: 1; seconds spent: 0.000041"
sent: 1; skipped: 0; total: 1
```

Del resultado obtenido, se puede identificar el resultado correcto con **0**  fallas
```sh
"response":"success","info":"processed: 1; failed: 0
```


## Dashboard

Puede adicioanr un widget a su Dashboard eligiendo "Data overview" and "application" "Linupdates-Panel"

![](img/zabbix-panel.png)
