#!/bin/bash

#Cambiar por el nombre del servidor registrado en Zabbix
SrvHost=$( cat /etc/zabbix/zabbix_agent2.conf | grep -v '^#' | awk -F\= '/Hostname/ { print $2 }' )

mkdir -p /tmp/zabbix

Sender="/usr/bin/zabbix_sender"
Senderarg1='-vv'
Senderarg2='-c'
Senderarg3="/etc/zabbix/zabbix_agent2.conf"
Senderarg4='-i'
Senderarg5='-k'
Senderarg6='-o'
Senderarg7='0'
Senderarg8='1'

Senderargos='/tmp/zabbix/os.txt'
Senderarglastkernel='/tmp/zabbix/lastkernel.txt'
Senderargkernel='/tmp/zabbix/kernel.txt'
Senderargcountupdate='/tmp/zabbix/countupdated.txt'
SenderargKernelReboot='/tmp/zabbix/kernelreboot.txt'
SenderargUpdateReboot='/tmp/zabbix/updatereboot.txt'

function detectOS {
    if [[ -e /etc/centos-release ]]; then
        export os="centos"
        vos=$( cat /etc/os-release | grep -w PRETTY_NAME | awk -F\= '{ print $2 }' |  sed 's/"//g' | sed 's/Linux//g' )
    fi
    if [[ -e /etc/debian_version ]]; then
        export os="debian"
        vos=$( cat /etc/os-release | grep -w PRETTY_NAME | awk -F\= '{ print $2 }' |  sed 's/"//g' | sed 's/GNU\/Linux//g' )
    fi
    echo "- Linupdates.OS "$vos > $Senderargos
}

function check_last_kernel {
    if [[ "$os" == "debian" ]]; then
        last_kernel=$( dpkg -l | egrep 'linux-image|pve-kernel' | grep ii | egrep -v 'meta-package|helper|firmware' | awk '{print $2}' | sed 's/linux-image-//g' | sed 's/pve-kernel-//g' | sort | tail -1 )
    fi
    if [[ "$os" == "centos" ]]; then
        #last_kernel=$( rpm -qa | grep -i kernel | head -n1 | sed 's/.elrepo.x86_64//g' )
        last_kernel=$( ls /boot/vmli* | grep -v rescue | sed 's/boot//g' | sed 's/.x86_64//g' | tail -n1 | sed 's/\///g' | sed 's/vmlinuz-//g' )
    fi
    export last_kernel
    echo "- Linupdates.LastKernel "$last_kernel > $Senderarglastkernel
}

function check_kernel {
    kernel=$( uname -r | sed 's/.x86_64//g' )
    export kernel
    echo "- Linupdates.Kernel "$kernel > $Senderargkernel
}

function check_OS_upgrades {
    if [[ "$os" == "debian" ]]; then
        if [[ "$update_needed" == "y" ]]; then
            apt update &>/dev/null
        fi

        pkg_to_update=$( apt list --upgradable 2>/dev/null | tail -n +2 | wc -l )
    fi

    if [[ "$os" == "centos" ]]; then
        yum_output=$(yum check-update --cacheonly -q --errorlevel=0)
        pkg_to_update=$(echo "$yum_output" | egrep -v '^(Load| \*|$)' | wc -l)
    fi
    echo "- Linupdates.updates "$pkg_to_update > $Senderargcountupdate
}

function check_reboot_kernel {
    if [[ "$os" == "debian" ]]; then
        last_kernel=$( echo $last_kernel | sed "s/amd64\(.*\).*/\1/"| tr -dc '0-9' )
        kernel=$( echo $kernel |  sed "s/amd64\(.*\).*/\1/" | tr -dc '0-9' )
    fi
    if [[ "$os" == "centos" ]]; then
        last_kernel=$( echo $last_kernel | sed "s/amd64\(.*\).*/\1/"| cut -d'-' -f 1 | tr -dc '0-9' )
        kernel=$( echo $kernel |  sed "s/amd64\(.*\).*/\1/" | cut -d'-' -f 1 | tr -dc '0-9' )
    fi
    echo $kernel
    echo $last_kernel
    vcount=0
    if [ "$kernel" -lt "$last_kernel" ]; then
        vcount=1
    fi
    echo "- Linupdates.KernelReboot "$vcount > $SenderargKernelReboot
}

# >  REST=$( /usr/sbin/needrestart -bk | grep NEEDRESTART-KSTA | awk '{ print $2}' )
#
# https://github.com/liske/needrestart/blob/master/README.batch.md
# The kernel status (NEEDRESTART-KSTA) value has the following meaning:
#0: unknown or failed to detect
#1: no pending upgrade
#2: ABI compatible upgrade pending
#3: version upgrade pending

function check_reboot {
    if [[ "$os" == "debian" ]]; then
        REST=$( /usr/sbin/needrestart -bk | grep NEEDRESTART-KSTA | awk '{ print $2}' )
        if [ 1 -lt $REST ]; then
            vreboot=1
        else
            vreboot=0
        fi
    fi
    if [[ "$os" == "centos" ]]; then
        vreboot=$( needs-restarting  -r | echo $? | tail -n1 )
    fi    
    echo "- Linupdates.Reboot "$vreboot > $SenderargUpdateReboot
}

function sender_zabbix {
    $Sender $Senderarg1 $Senderarg2 $Senderarg3 $Senderarg4 $Senderargos -s "$SrvHost"
    $Sender $Senderarg1 $Senderarg2 $Senderarg3 $Senderarg4 $Senderarglastkernel -s "$SrvHost"
    $Sender $Senderarg1 $Senderarg2 $Senderarg3 $Senderarg4 $Senderargkernel -s "$SrvHost"
    $Sender $Senderarg1 $Senderarg2 $Senderarg3 $Senderarg4 $Senderargcountupdate -s "$SrvHost"
    $Sender $Senderarg1 $Senderarg2 $Senderarg3 $Senderarg4 $SenderargKernelReboot -s "$SrvHost"
    $Sender $Senderarg1 $Senderarg2 $Senderarg3 $Senderarg4 $SenderargUpdateReboot -s "$SrvHost"
}

function main(){
    detectOS
    check_last_kernel
    check_kernel
    check_OS_upgrades
    check_reboot
    check_reboot_kernel
    sender_zabbix
}

main
